import "./App.css";
import Login from "./component/auth/Login";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Main from "./component/layout/Main";
import Admin from "./component/layout/Admin";
import Dashboard from "./component/Dashboard";
import Register from "./component/auth/Register";
import UbahData from "./component/CRUD/UbahData";

function App() {

  const AgentAuth = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={props => (
        <Main>
          <Component {...props} />
        </Main>
      )} />
    )
  }
  const AgentDashboard = ({ component: Component, ...rest }) => {
    return (
      <Route {...rest} render={props => (
        <Admin>
          <Component {...props} />
        </Admin>
      )} />
    )
  }

  return (
    <div className="App">
      <div className="full-height">


        <Router>
          <Switch>
            <AgentAuth exact path={["/", "/login"]} component={Login} />
            <AgentAuth path="/register" component={Register} />
            <AgentDashboard path="/dashboard" component={Dashboard} />
            <AgentDashboard path="/ubahdata" component={UbahData} />
          </Switch>
        </Router>
      </div>
    </div>
  );
}
{
  /* <Route path="/admin/:path?" exact />
<Admin>
  <Switch>
    <Route path="/admin/dashboard" component={Dashboard} />
    <Route path="/admin/profile" component={Profile} />
  </Switch>
</Admin> */
}

export default App;
