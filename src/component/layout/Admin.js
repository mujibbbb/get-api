import React from "react";
import NavbarAdmin from './NavbarAdmin';
import { Container, Col } from "reactstrap";

const Admin = (props) => {
  return (
    <div>
      <NavbarAdmin />
      <Container fluid={true} className="mt-2">
        <Col>{props.children}</Col>
      </Container>
    </div>
  );
};

export default Admin;
