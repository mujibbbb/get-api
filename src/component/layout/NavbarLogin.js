import React, { useState } from "react";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import { NavLink as RRNavLink } from "react-router-dom";

const NavbarLogin = (props) => {
  const [isOpen, setOpen] = useState(false);
  const toggle = () => setOpen(!isOpen);

  return (
    <div>
      <Navbar>
        <NavbarBrand href={"/"}></NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav navbar>
            <NavItem>
              <NavLink to="/" tag={RRNavLink}>
                Login
                </NavLink>
            </NavItem>
            <NavItem>
              <NavLink to="/register" tag={RRNavLink}>
                Register
                </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </div>

  );
};

export default NavbarLogin;
