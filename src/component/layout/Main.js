import React from "react";
import NavbarLogin from "./NavbarLogin";
import { Container, Col } from "reactstrap";

const Main = (props) => {
  return (
    <div>
      <NavbarLogin />
      <Container fluid={true} className="mt-2">
        <Col>{props.children}</Col>
      </Container>
    </div>
  );
};

export default Main;
