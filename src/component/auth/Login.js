import React, { useState, useEffect } from "react"; 
import {
  useHistory,
  Link,
} from "react-router-dom";
import axios from "axios";
import {
  Col,
  Container,
  Button,
  Form,
  FormGroup,
  Input,
  Row,
} from "reactstrap";
import './Login.css';
import Loading from 'react-spinners/SyncLoader';

const Login = () => {

  const history = useHistory();
  const initialState = {
    username: '',
    password: '',
  };

  const [state, setState] = useState(initialState);
  const [isEnabled, setEnabled] = useState(false);

   //loading spinner
   const [loading, setLoading] = useState(false)

  useEffect(() => {
    setEnabled(state.username !== '' && state.password !== '' ? false : true);
  }, [state]);
  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);
    axios.post(
        "https://coba-heroku-nodejs.herokuapp.com/api/auth/signin",
        state
        //callback
      )
      .then((response) => {
      
        sessionStorage.setItem("token", response.data.accessToken);
        console.log(response.data);
        sessionStorage.setItem("role", response.data.roles);

        //push ke dashboard
       
        history.push('/dashboard');
        console.log(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <div className="home-login">
      <Form onSubmit={(e) => handleSubmit(e)}>
        <Container className="themed-container">
          <Row>
            <Col sm="6">
              <h1>Facebook</h1> Facebook membantu Anda terhubung dan berbagi
              dengan orang-orang dalam kehidupan Anda.
            </Col>
            <Col sm="6">
              <FormGroup>
                <Input
                  type="text"
                  name="username"
                  placeholder="Email atau no Telepon"
                  onChange={(e) => setState((prevState) => {
                    prevState.username = e.target.value;
                    return {
                      ...prevState,
                    };
                  })
                  }
                />
              </FormGroup>
              <FormGroup>
                <Input
                  type="password"
                  name="password"
                  placeholder="Masukan Password"
                  onChange={(e) => setState((prevState) => {
                    prevState.password = e.target.value;
                    return {
                      ...prevState,
                    };
                  })
                  }
                />
              </FormGroup>
            </Col>
          </Row>
          <Row>
            <Col sm="6">

            </Col>

            <Col sm="6">
              <Button 
              color="primary" 
              size="lg" 
              block disabled={isEnabled}
              position="fixed"
              >
                 { loading ? <Loading /> : 'Login' }
              </Button>
              <Link to="/register">keregister</Link>
            </Col>
          </Row>


        </Container>
      </Form>


    </div>

  );
};

export default Login;
