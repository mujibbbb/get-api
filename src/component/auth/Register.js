import React, { useState, useEffect } from "react";
import {
  Container,
  Form,
  Card,
  FormControl,
  Button,
  FormGroup
} from "react-bootstrap";
import { useHistory, Link } from 'react-router-dom';
import axios from "axios";

const Register = () => {
  const history = useHistory();
  const initialState = {
    name: "",
    username: "",
    email: "",
    password: "",
  };
  const [isEnabled, setEnabled] = useState(false);
  const [state, setState] = useState(initialState);
  useEffect(() => {
    setEnabled(state.username !== '' && state.password !== '' ? false : true);
  }, [state]);
  const handleSubmit = async (e) => {
    e.preventDefault();
    axios.post(
        "https://coba-heroku-nodejs.herokuapp.com/api/auth/signupuser",
        state
        //callback
      )
      .then((response) => {
        //push ke dashboard
        history.push('/login');
        console.log(response);
      })
      .catch((error) => {
        console.error(error);
      });
  };





  return (
    <div>
      <Container>
        <div className="d-flex justify-content-center mt-4">
          <Card>
            <Card.Body>
              <Form onSubmit={(e) => handleSubmit(e)}>
                <Form.Group>
                  <FormControl
                    type="text"
                    value={state.name}
                    placeholder="Nama"
                    onChange={(e) =>
                      setState((prevState) => {
                        prevState.name = e.target.value;
                        return {
                          ...prevState,
                        };
                      })
                    }
                  ></FormControl>
                </Form.Group>
                {/* batas form group */}
                <Form.Group>
                  <FormControl
                    type="text"
                    value={state.username  || ''}
                    placeholder="Username"
                    onChange={(e) =>
                      setState((prevState) => {
                        prevState.username = e.target.value;
                        return {
                          ...prevState,
                        };
                      })
                    }
                  ></FormControl>
                </Form.Group>
                {/* batas form group */}
                <FormGroup>
                  <FormControl
                    type="text"
                    value={state.email || ''}
                    placeholder="Masukan Email"
                    onChange={(e) =>
                      setState((prevState) => {
                        prevState.email = e.target.value;
                        return {
                          ...prevState,
                        };
                      })
                    }
                  ></FormControl>
                </FormGroup>
                {/* batas form group */}
                <FormGroup>
                  <FormControl
                    type="password"
                    value={state.password || ''}
                    placeholder="Kata sandi"
                    onChange={(e) =>
                      setState((prevState) => {
                        prevState.password = e.target.value;
                        return {
                          ...prevState,
                        };
                      })
                    }
                  ></FormControl>
                </FormGroup>
                {/* batas form group */}
                <Button type="submit" block >
                  Register
                </Button>
              </Form>
            </Card.Body>
          </Card>
        </div>
      </Container>
      <Link to="/login">
        kelogin
        </Link>
    </div>
  );
};

export default Register;
