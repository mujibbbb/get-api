import React from 'react';
import { Redirect } from 'react-router-dom';

const Protected = props => {
    const Component = props.component;
    const isAuthentication = sessionStorage.getItem('token');
    return  isAuthentication ? (
        <Component />
    ): (
        <Redirect to={{
            pathname: '/login'
        }} />
    )
}

export default Protected
