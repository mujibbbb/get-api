import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Table, Button } from 'reactstrap';
import { useHistory } from 'react-router-dom';
import SyncLoader from 'react-spinners/RingLoader';

const Dashboard = () => {
    const history = useHistory();
    const url = 'https://coba-heroku-nodejs.herokuapp.com/api/admin/semuadata';
    const [datauser, setData] = useState([]);
    const [hapus, setHapus] = useState(false);
    const [token, setToken] = useState(sessionStorage.getItem('token'));

    //loading spinner
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const Api = async () => {
            axios.get(url, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then((response) => {
                    setLoading(false);
                    // console.log(token);
                    // console.log(response.data.user);
                    setData(response.data.user);
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        if (token !== null)
            Api();
    }, [token, hapus]);

    //hapus data dari API
    const hapusData = (id) => {
        axios.delete(`https://coba-heroku-nodejs.herokuapp.com/api/delete/${id}`, {
            headers: {
                Authorization: `Bearer ${token}`
            },
        })
            .then(response => {
                setHapus(!hapus);
            })
            .catch((error) => {
                console.error(error);
            })
    }

    //push ke file ubah data
    const ubahData = (id) => {
        history.push(
            '/ubahdata', id
        );
    }
    const style = { position: "fixed", top: "50%", left: "50%", transform: "translate(-50%, -50%) " };
    const ShowUser = () => {
        return datauser.map(item => {
            return (
                <tr>
                    <td key={item.roles.id}></td>
                    <td>1</td>
                    <td>{item.name}</td>
                    <td>{item.username}</td>
                    <td>{item.email}</td>
                    <td>
                        <Button outline color="danger" onClick={() => hapusData(item.id)}>Hapus</Button> | <Button outline color="success" onClick={() => ubahData(item.id)}>Edit</Button>
                    </td>
                </tr>

            )
        })
    }
    return (
        <Table hover>
            <thead>
                <tr>
                    <th></th>
                    <th>No</th>
                    <th>Name</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {loading ?
                    <div style={style}>
                <SyncLoader />  
                </div> :
                ShowUser()
                }
            </tbody>
        </Table >
    );
}





// class Dashboard extends Component {
//     componentDidMount() {
//         const url = 'https://coba-heroku-nodejs.herokuapp.com/api/admin/semuadata'
//         axios.get(url)
//             .then(data => {
//                 console.log(data);
//             })
//     }
//     render() {
//         console.log('1 sedang dipasang');
//         return (
//             <div>

//             </div>
//         )
//     }
// }

export default Dashboard
