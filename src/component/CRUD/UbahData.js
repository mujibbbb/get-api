import React, { useEffect, useState } from 'react';
import {
    Container,
    Form,
    Card,
    FormControl,
    FormGroup
} from "react-bootstrap";
import { useHistory } from "react-router-dom";
import axios from "axios";
import { Button } from 'reactstrap';
import './UbahData.css';

const UbahData = (props) => {
    const history = useHistory();
    const id = props.location.state;

    const kembali = () => {
        history.push("/dashboard")
    }
    const [state, setState] = useState({});
    const token = sessionStorage.getItem('token');
    

    useEffect(() => {
        const Api = async () => {
            axios.get(`https://coba-heroku-nodejs.herokuapp.com/api/admin/semuadata/${id}`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
                .then((response) => {
                    // console.log(token);
                    // console.log(response.data.user);
                    setState(response.data.user);
                    
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        if (token !== null)
            Api();
    }, [token]);

    const sumbit = (e) => {
        e.preventDefault();
        const data = {
            name: state.name,
            username: state.username,
            email: state.email,
        }

        axios.put(`https://coba-heroku-nodejs.herokuapp.com/api/update/${id}`, 
        data, 
        {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then((response) => {
                alert('Berhasil di Update');
                console.log(response.data);
                history.push('/dashboard');
            })
            .catch((error) => {
                console.error(error);
            })

    }
  
    return (
        <div>
            <Container>
                <Card>
                    <Card.Body>
                        <Form onSubmit={(e) => sumbit(e)}
                        >
                            <h2>was sampun</h2>
                            <FormGroup>
                                <FormControl
                                    type="text"
                                    value={state.name || ''}
                                    placeholder="Masukan Nama Baru"
                                    onChange={(e) =>
                                        setState((prevState) => {
                                            prevState.name = e.target.value;
                                            return {
                                                ...prevState,
                                            };
                                        })}
                                >
                                </FormControl>
                            </FormGroup>
                            {/* batas form group */}
                            <FormGroup>
                                <FormControl
                                    type="text"
                                    value={state.username || ''}
                                    placeholder="Masukan Email Baru"
                                    onChange={(e) =>
                                        setState((prevState) => {
                                            prevState.username = e.target.value;
                                            return {
                                                ...prevState,
                                            };
                                        })}
                                >
                                </FormControl>
                            </FormGroup>
                            {/* batas form group */}
                            <FormGroup>
                                <FormControl
                                    type="text"
                                    value={state.email || ''}
                                    placeholder="Masukan Email Baru"
                                    onChange={(e) =>
                                        setState((prevState) => {
                                            prevState.email = e.target.value;
                                            return {
                                                ...prevState,
                                            };
                                        })}
                                >
                                </FormControl>
                            </FormGroup>
                            {/* batas form group */}

                            <Button outline type="submit" color="dark" size="lg" onClick={() => kembali()}>
                                Kembali
                            </Button> | <Button  outline type="submit" color="success" size="lg"> Ubah </Button>
                        </Form>
                    </Card.Body>
                </Card>
            </Container>
        </div>
    )
}

export default UbahData
